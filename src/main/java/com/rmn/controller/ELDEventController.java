package com.rmn.controller;

import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rmn.custom.dto.CustomUser;
import com.rmn.custom.dto.CustomUserRequest;
import com.rmn.dto.EldEventDto;
import com.rmn.service.CanadaEldFileService;
import com.rmn.service.EldEventService;

@RestController
public class ELDEventController {

	Logger logger = LoggerFactory.getLogger(ELDEventController.class);

	@Autowired
	private EldEventService eldEventService;

	@Autowired
	private CanadaEldFileService CanadaEldFileService;

	@GetMapping("/hello")
	public String greeting() {
		logger.info("Hello Controller called  ", new Timestamp(System.currentTimeMillis()));
		return "Good";
	}

	@GetMapping("/query")
	public List<EldEventDto> query() {
		List<EldEventDto> list = eldEventService.getList();
		System.out.println(list.get(0).getLastName());
		return list;
	}

	@GetMapping("/eld")
	public List<CustomUser> eldList() {
		List<CustomUser> list = CanadaEldFileService.getUserList();
		System.out.println(list);
		return list;
	}

	@PostMapping("/elds")
	public List<CustomUser> eldEventList(@RequestBody CustomUserRequest customUser) {
		logger.info("Time before calling service {} ", new Timestamp(System.currentTimeMillis()));
		List<CustomUser> list = CanadaEldFileService.getUserList(customUser);

		logger.info("Time we get the output:  {} : {} ", new Timestamp(System.currentTimeMillis()), list);
		return list;
	}
}
