package com.rmn.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rmn.dto.EldEventDto;
import com.rmn.repository.EldEventRepository;
import com.rmn.service.EldEventService;

@Service
public class EldEventServiceImpl implements EldEventService {

	@Autowired
	private EldEventRepository eldEventRepository;

	@Override
	public List<EldEventDto> getList() {
		System.out.println(eldEventRepository.getEldEventDtoList());
		return eldEventRepository.getEldEventDtoList();
	}

}
