package com.rmn.serviceImpl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.rmn.custom.dto.CustomUser;
import com.rmn.custom.dto.CustomUserRequest;
import com.rmn.repository.CanadaEldFileRepository;
import com.rmn.service.CanadaEldFileService;

@Service
public class CanadaEldFileServiceImpl implements CanadaEldFileService {

	@Autowired
	private CanadaEldFileRepository canadaEldFileRepository;

	@Override
	public List<CustomUser> getUserList() {
		Long l1 = 1604642400000l;
		Long l2 = 1604901600000l;

		List<CustomUser> list = new ArrayList<>();
		// 1st query
		List<CustomUser> list1 = canadaEldFileRepository.getList(l1, l2, 3346, "ufagaa");
		if (!CollectionUtils.isEmpty(list1)) {
			list.addAll(list1);
		}
		// 2nd query
		List<CustomUser> list2 = canadaEldFileRepository.getList2(l1, l2, 1);
		if (!CollectionUtils.isEmpty(list2)) {
			list.addAll(list2);
		}
		return list;
	}

	@Override
	public List<CustomUser> getUserList(CustomUserRequest user) {
		List<CustomUser> list = new ArrayList<>();
		// 1st query
		List<CustomUser> list1 = canadaEldFileRepository.getList(user.getFromEventTs(), user.getToEventTs(),
				user.getCompanyId(), user.getTractorNumberForUserList());
		if (!CollectionUtils.isEmpty(list1)) {
			list.addAll(list1);
		}

		// 2nd query
		List<CustomUser> list2 = canadaEldFileRepository.getList2(user.getFromEventTs(), user.getToEventTs(),
				user.getUserId());
		if (!CollectionUtils.isEmpty(list2)) {
			list.addAll(list2);
		}
		Long longData = canadaEldFileRepository.getList3(user.getFromEventTs(), user.getToEventTs(),
				user.getTractorNumberForUserList(), user.getCompanyId());
		if (longData != null) {
			System.err.println("user " + longData);
			CustomUser customUser = new CustomUser();
			customUser.setEventTs(longData);
			customUser.setUserFirstName("Undefined");
			customUser.setUserLastName("Undefined");
			customUser.setAccountType("D");
			list.add(customUser);
		}

		List<CustomUser> sortedList = list.stream().sorted(Comparator.comparingLong(CustomUser::getEventTs).reversed())
				.collect(Collectors.toList());

		return sortedList;
	}
}
