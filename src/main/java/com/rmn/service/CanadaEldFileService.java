package com.rmn.service;

import java.util.List;

import com.rmn.custom.dto.CustomUser;
import com.rmn.custom.dto.CustomUserRequest;

public interface CanadaEldFileService {

	public List<CustomUser> getUserList();

	public List<CustomUser> getUserList(CustomUserRequest user);

}
