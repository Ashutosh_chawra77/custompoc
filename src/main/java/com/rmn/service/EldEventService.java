package com.rmn.service;

import java.util.List;

import com.rmn.dto.EldEventDto;

public interface EldEventService {

	public List<EldEventDto> getList();
}
