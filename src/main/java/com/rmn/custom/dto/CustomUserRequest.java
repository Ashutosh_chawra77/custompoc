package com.rmn.custom.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CustomUserRequest {

	int companyId;
	long fromEventTs;
	long toEventTs;
	String tractorNumberForUserList;
	int userId;

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public long getFromEventTs() {
		return fromEventTs;
	}

	public void setFromEventTs(long fromEventTs) {
		this.fromEventTs = fromEventTs;
	}

	public long getToEventTs() {
		return toEventTs;
	}

	public void setToEventTs(long toEventTs) {
		this.toEventTs = toEventTs;
	}

	public String getTractorNumberForUserList() {
		return tractorNumberForUserList;
	}

	public void setTractorNumberForUserList(String tractorNumberForUserList) {
		this.tractorNumberForUserList = tractorNumberForUserList;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
