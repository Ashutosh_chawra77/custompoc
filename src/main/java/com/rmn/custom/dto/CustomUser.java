package com.rmn.custom.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CustomUser {

	int userId;
	int company;
	String userFirstName;
	String userLastName;
	long eventTs;
	int role;
	String accountType;

	public CustomUser() {

	}

	public CustomUser(int userId, int company, String userFirstName, String userLastName, long eventTs, int role) {
		this.userId = userId;
		this.company = company;
		this.userFirstName = userFirstName;
		this.userLastName = userLastName;
		this.eventTs = eventTs;
		this.role = role;

	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getCompany() {
		return company;
	}

	public void setCompany(int company) {
		this.company = company;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public long getEventTs() {
		return eventTs;
	}

	public void setEventTs(long eventTs) {
		this.eventTs = eventTs;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
