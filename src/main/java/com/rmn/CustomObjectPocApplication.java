package com.rmn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@ComponentScan("com.rmn")
public class CustomObjectPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomObjectPocApplication.class, args);
	}

}
