package com.rmn.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.rmn.custom.dto.CustomUser;
import com.rmn.dto.User;

@Repository
public interface CanadaEldFileRepository extends CrudRepository<User, Long> {

	// case when user_role_id = '3' then 'D' else 'S' end as accountType, \"\r\n"
	@Query("select new com.rmn.custom.dto.CustomUser(ru.userId, ru.company.companyId,  ru.userFirstName, ru.userLastName ,  max(ree.eventTs), ru.role.id)  from User ru  "
			+ "		 left outer join EldEvents ree on ru.userId = ree.userId "
			+ "		where ru.company.companyId = :companyId and  "
			+ "	 ru.userId in  (select re.userId from EldEvents re where re.eventTs between "
			+ "	 :fromDateTS  AND :toDateTS  and re.companyId = :companyId "
			+ "	 and re.tractorNumber = :tractorNumberForUserList ) "
			+ "	 and ree.eventTs between :fromDateTS  AND  :toDateTS  group by ree.userId ")
	public List<CustomUser> getList(@Param("fromDateTS") Long fromDateTS, @Param("toDateTS") Long toDateTS,
			@Param("companyId") int companyId, @Param("tractorNumberForUserList") String tractorNumberForUserList);

// select * from rm_user ru 
//	left outer join rm_eld_events ree on ru.user_id = ree.eld_events_record_originator_id where ree.eld_events_record_origin = 3 and ree.eld_events_timestamp between 1602651600000 
	// AND 520335305 and ree.eld_events_driver_id= 1 group by
	// ree.eld_events_record_originator_id;

	// case when user_role_id = '3' then 'D' else 'S' end as AccountType, "
	@Query("select new com.rmn.custom.dto.CustomUser(ru.userId, ru.company.companyId,  ru.userFirstName, ru.userLastName ,  max(ree.eventTs), ru.role.id)  from User ru "
			+ " left outer join EldEvents ree on ru.userId = ree.recordOriginatorId "
			+ " where ree.recordOrigin = 3 and ree.eventTs between :fromDateTS "
			+ " AND  :toDateTS and ree.userId= :userId  group by ree.recordOriginatorId ")
	public List<CustomUser> getList2(@Param("fromDateTS") Long fromDateTS, @Param("toDateTS") Long toDateTS,
			@Param("userId") int userId);

	@Query("select  max(eventTs) from EldEvents  where  eventTs between :fromDateTS AND :toDateTS and "
			+ "companyId = :companyId and tractorNumber = :tractorNumberForUserList"
			+ " and userId = -1  and eventTs is not null")
	public Long getList3(@Param("fromDateTS") Long fromDateTS, @Param("toDateTS") Long toDateTS,
			@Param("tractorNumberForUserList") String tractorNumberForUserList, @Param("companyId") int companyId);

}
