package com.rmn.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.rmn.dto.CustomEmployee;

@Repository
public interface CustomEmployeeRepository extends CrudRepository<CustomEmployee, Long> {

}
