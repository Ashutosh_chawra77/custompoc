package com.rmn.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.rmn.dto.EldEventDto;

@Repository
public interface EldEventRepository extends CrudRepository<EldEventDto, Long> {

	@Query(value = "select c from EldEventDto c")
	public List<EldEventDto> getEldEventDtoList();
}
