package com.rmn.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "rm_eld_events_description")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class EldEventsDescription {

    @Id
    @Column(name = "eld_events_description_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "eld_events_description_type")
    private short eldEventsDescriptionType;
    
    @Column(name = "eld_events_description_code")
    private short eldEventsDescriptionCode;
    
    @Column(name = "eld_events_description_mal_diag_code")
    private String eldEventsDescriptionMalDiagCode;
    
    @Column(name = "eld_events_description_status_description")
    private String eldEventsDescriptionStatusDescription;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public short getEldEventsDescriptionType() {
        return eldEventsDescriptionType;
    }

    public void setEldEventsDescriptionType(short eldEventsDescriptionType) {
        this.eldEventsDescriptionType = eldEventsDescriptionType;
    }

    public short getEldEventsDescriptionCode() {
        return eldEventsDescriptionCode;
    }

    public void setEldEventsDescriptionCode(short eldEventsDescriptionCode) {
        this.eldEventsDescriptionCode = eldEventsDescriptionCode;
    }

    public String getEldEventsDescriptionMalDiagCode() {
        return eldEventsDescriptionMalDiagCode;
    }

    public void setEldEventsDescriptionMalDiagCode(String eldEventsDescriptionMalDiagCode) {
        this.eldEventsDescriptionMalDiagCode = eldEventsDescriptionMalDiagCode;
    }

    public String getEldEventsDescriptionStatusDescription() {
        return eldEventsDescriptionStatusDescription;
    }

    public void setEldEventsDescriptionStatusDescription(String eldEventsDescriptionStatusDescription) {
        this.eldEventsDescriptionStatusDescription = eldEventsDescriptionStatusDescription;
    }
    
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
