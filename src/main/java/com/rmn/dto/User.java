package com.rmn.dto;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "rm_user")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class User {

	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;

	@Column(name = "user_first_name")
	private String userFirstName;

	@Column(name = "user_last_name")
	private String userLastName;

	@Column(name = "user_contact_number")
	private String userNumber;

	@Column(name = "user_verified_email")
	private String userVerifiedEmail;

	@Column(name = "user_contact_email")
	private String userEmail;

	@Column(name = "user_contact_email_is_verified")
	private short userEmailIsVerified;

	@Column(name = "user_password")
	private String userPassword;

	@Column(name = "user_access_token")
	private String userAccessToken;

	@Column(name = "user_terminal_id")
	private int userTerminalId;

	@Column(name = "user_trip_id")
	private int userTripId;

	@Column(name = "user_is_driver")
	private short userIsDriver;

	@Column(name = "user_is_logged_in")
	private short userIsLoggedIn;

	@Column(name = "user_email_verification_validity")
	private long userEmailVerificationValidity;

	@Column(name = "user_driver_login_id")
	private String userDriverLoginId;

	@Column(name = "user_tractor_number")
	private String userTractorNumber;

	@Column(name = "user_trailer_number")
	private String userTrailerNumber;

	@Column(name = "user_hos_rule")
	private String userHoSRule;

	@Column(name = "user_tz")
	private String userTz;

	@Column(name = "user_is_dst")
	private boolean userIsDst;

	@Column(name = "user_is_tz_changed")
	private short userIsTzChanged;

	@Column(name = "user_device_id")
	private int userDeviceId;

	@Column(name = "user_device_paired_date")
	private long userDevicePairedDate;

	@Column(name = "user_msg_jid")
	private String userMsgJID;

	@Column(name = "user_exemption_id")
	private int userExemptionId;

	@Column(name = "user_app_version")
	private String userAppVersion;

	@Column(name = "user_last_file_sync_update")
	private long userLastFileSyncUpdate;

	@Column(name = "user_name_on_license")
	private String dlName; // name as on driving license

	@Column(name = "user_license_number")
	private String dlNum; // driving license number

	@Column(name = "user_license_state")
	private int dlSt; // driving license - state of jurisdiction

	@Column(name = "user_license_is_verified")
	private short dlVerified;

	@Column(name = "user_shipping_document_number")
	private String userShippingDocumentNumber;

	@ManyToOne
	@JoinColumn(name = "user_role_id", nullable = false, referencedColumnName = "role_id")
	private Role role;

	@ManyToOne
	@JoinColumn(name = "user_company_id", nullable = false, referencedColumnName = "company_id")
	private Company company;

	@Column(name = "user_is_active")
	private short userIsActive;

	@Column(name = "user_app_db_sync")
	private boolean userAppDbSync;

	private short status;

	// below column fields (user_client_id,user_created_by,user_created_dt) have
	// been added for audit changes
	@Column(name = "user_client_id")
	private int userClientId;

	@Column(name = "user_created_by")
	private int userCreatedBy;

	@Column(name = "user_created_dt", insertable = false, updatable = false)
	private Timestamp userCreatedDt;

	@Column(name = "user_dot_number")
	private String userDOTNumber;

	@Column(name = "user_oa_number")
	private String userOANumber;

	@Column(name = "user_last_sign_in_date_time")
	private String userLastSignInDateTime;

	@Column(name = "user_last_sign_out_date_time")
	private String userLastSignOutDateTime;

	@Column(name = "user_last_sign_in_location")
	private String userLastSignInLocation;

	@Column(name = "user_last_sign_out_location")
	private String userLastSignOutLocation;

	@Column(name = "user_changed_by")
	private int changedBy;

	@Transient
	private int moaId = 0;

	public int getUserClientId() {
		return userClientId;
	}

	public void setUserClientId(int userClientId) {
		this.userClientId = userClientId;
	}

	public int getUserCreatedBy() {
		return userCreatedBy;
	}

	public void setUserCreatedBy(int userCreatedBy) {
		this.userCreatedBy = userCreatedBy;
	}

	public Timestamp getUserCreatedDt() {
		return userCreatedDt;
	}

	public void setUserCreatedDt(Timestamp userCreatedDt) {
		this.userCreatedDt = userCreatedDt;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}

	public String getUserVerifiedEmail() {
		return userVerifiedEmail;
	}

	public void setUserVerifiedEmail(String userVerifiedEmail) {
		this.userVerifiedEmail = userVerifiedEmail;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public short getUserEmailIsVerified() {
		return userEmailIsVerified;
	}

	public void setUserEmailIsVerified(short userEmailIsVerified) {
		this.userEmailIsVerified = userEmailIsVerified;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserAccessToken() {
		return userAccessToken;
	}

	public void setUserAccessToken(String userAccessToken) {
		this.userAccessToken = userAccessToken;
	}

	public short getUserIsDriver() {
		return userIsDriver;
	}

	public void setUserIsDriver(short userIsDriver) {
		this.userIsDriver = userIsDriver;
	}

	public short getUserIsLoggedIn() {
		return userIsLoggedIn;
	}

	public int getUserTerminalId() {
		return userTerminalId;
	}

	public void setUserTerminalId(int userTerminalId) {
		this.userTerminalId = userTerminalId;
	}

	public int getUserTripId() {
		return userTripId;
	}

	public void setUserTripId(int userTripId) {
		this.userTripId = userTripId;
	}

	public void setUserIsLoggedIn(short userIsLoggedIn) {
		this.userIsLoggedIn = userIsLoggedIn;
	}

	public long getUserEmailVerificationValidity() {
		return userEmailVerificationValidity;
	}

	public void setUserEmailVerificationValidity(long userEmailVerificationValidity) {
		this.userEmailVerificationValidity = userEmailVerificationValidity;
	}

	public String getUserDriverLoginId() {
		return userDriverLoginId;
	}

	public void setUserDriverLoginId(String userDriverLoginId) {
		this.userDriverLoginId = userDriverLoginId;
	}

	public String getUserTractorNumber() {
		return userTractorNumber;
	}

	public void setUserTractorNumber(String userTractorNumber) {
		this.userTractorNumber = userTractorNumber;
	}

	public String getUserTrailerNumber() {
		return userTrailerNumber;
	}

	public void setUserTrailerNumber(String userTrailerNumber) {
		this.userTrailerNumber = userTrailerNumber;
	}

	public String getUserHoSRule() {
		return userHoSRule;
	}

	public void setUserHoSRule(String userHoSRule) {
		this.userHoSRule = userHoSRule;
	}

	public String getUserTz() {
		return userTz;
	}

	public void setUserTz(String userTz) {
		this.userTz = userTz;
	}

	public boolean isUserIsDst() {
		return userIsDst;
	}

	public void setUserIsDst(boolean userIsDst) {
		this.userIsDst = userIsDst;
	}

	public short getUserIsTzChanged() {
		return userIsTzChanged;
	}

	public void setUserIsTzChanged(short userIsTzChanged) {
		this.userIsTzChanged = userIsTzChanged;
	}

	public int getUserDeviceId() {
		return userDeviceId;
	}

	public void setUserDeviceId(int userDeviceId) {
		this.userDeviceId = userDeviceId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public short getUserIsActive() {
		return userIsActive;
	}

	public void setUserIsActive(short userIsActive) {
		this.userIsActive = userIsActive;
	}

	public boolean isUserAppDbSync() {
		return userAppDbSync;
	}

	public void setUserAppDbSync(boolean userAppDbSync) {
		this.userAppDbSync = userAppDbSync;
	}

	public long getUserDevicePairedDate() {
		return userDevicePairedDate;
	}

	public void setUserDevicePairedDate(long userDevicePairedDate) {
		this.userDevicePairedDate = userDevicePairedDate;
	}

	public String getUserMsgJID() {
		return userMsgJID;
	}

	public void setUserMsgJID(String userMsgJID) {
		this.userMsgJID = userMsgJID;
	}

	public int getUserExemptionId() {
		return userExemptionId;
	}

	public void setUserExemptionId(int userExemptionId) {
		this.userExemptionId = userExemptionId;
	}

	public String getUserAppVersion() {
		return userAppVersion;
	}

	public void setUserAppVersion(String userAppVersion) {
		this.userAppVersion = userAppVersion;
	}

	public String getDlName() {
		return dlName;
	}

	public void setDlName(String dlName) {
		this.dlName = dlName;
	}

	public String getDlNum() {
		return dlNum;
	}

	public void setDlNum(String dlNum) {
		this.dlNum = dlNum;
	}

	public int getDlSt() {
		return dlSt;
	}

	public void setDlSt(int dlSt) {
		this.dlSt = dlSt;
	}

	public short getDlVerified() {
		return dlVerified;
	}

	public void setDlVerified(short dlVerified) {
		this.dlVerified = dlVerified;
	}

	public long getUserLastFileSyncUpdate() {
		return userLastFileSyncUpdate;
	}

	public void setUserLastFileSyncUpdate(long userLastFileSyncUpdate) {
		this.userLastFileSyncUpdate = userLastFileSyncUpdate;
	}

	public String getUserShippingDocumentNumber() {
		return userShippingDocumentNumber;
	}

	public void setUserShippingDocumentNumber(String userShippingDocumentNumber) {
		this.userShippingDocumentNumber = userShippingDocumentNumber;
	}

	public String getUserDOTNumber() {
		return userDOTNumber;
	}

	public void setUserDOTNumber(String userDOTNumber) {
		this.userDOTNumber = userDOTNumber;
	}

	public String getUserOANumber() {
		return userOANumber;
	}

	public void setUserOANumber(String userOANumber) {
		this.userOANumber = userOANumber;
	}

	public String getUserLastSignInDateTime() {
		return userLastSignInDateTime;
	}

	public void setUserLastSignInDateTime(String userLastSignInDateTime) {
		this.userLastSignInDateTime = userLastSignInDateTime;
	}

	public String getUserLastSignOutDateTime() {
		return userLastSignOutDateTime;
	}

	public void setUserLastSignOutDateTime(String userLastSignOutDateTime) {
		this.userLastSignOutDateTime = userLastSignOutDateTime;
	}

	public String getUserLastSignInLocation() {
		return userLastSignInLocation;
	}

	public void setUserLastSignInLocation(String userLastSignInLocation) {
		this.userLastSignInLocation = userLastSignInLocation;
	}

	public String getUserLastSignOutLocation() {
		return userLastSignOutLocation;
	}

	public void setUserLastSignOutLocation(String userLastSignOutLocation) {
		this.userLastSignOutLocation = userLastSignOutLocation;
	}

	public int getChangedBy() {
		return changedBy;
	}

	public void setChangedBy(int changedBy) {
		this.changedBy = changedBy;
	}

	public int getMoaId() {
		return moaId;
	}

	public void setMoaId(int moaId) {
		this.moaId = moaId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
