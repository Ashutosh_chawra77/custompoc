package com.rmn.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "rm_company")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class Company {

	@Id
	@Column(name = "company_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int companyId;

	@Column(name = "company_code")
	private String companyCode;

	@Column(name = "company_name")
	@Size(max = 120, message = "Company name length should between 4 and 120")
	@Pattern(regexp = "^$|[a-zA-Z0-9_ .&/-]*", message = "Company name should contain Alpha numeric or space or & or _ or â€“ or .")
	private String companyName;

	@Column(name = "company_address")
	@Size(max = 100, message = "Company address length should be between 2 and 100")
	private String companyStreetAddress;

	@Column(name = "company_city")
	@Size(max = 50, message = "Company city length should be between 2 and 50")
	@Pattern(regexp = "^$|^[a-zA-Z]+([ a-zA-Z]+)*$", message = "Company city can contain only alphabet and space in between words")
	private String companyCity;

	@Column(name = "company_state")
	@Size(max = 50, message = "Company state length should be between 2 and 50")
	private String companyStateProvince;

	@Column(name = "company_zip")
	@Size(max = 10, message = "Company zip code length should be between 5 and 10")
	/*
	 * @Pattern(regexp = "^$|[0-9]*", message =
	 * "Company zip code should contain only Numeric")
	 */
	private String companyZipCode;

	@Column(name = "company_country")
	@Size(max = 50, message = "Company country length should be between 2 and 50")
	@Pattern(regexp = "^$|^[a-zA-Z]+( [a-zA-Z]+)*$", message = "Company country can contain only alphabet and space in between words")
	private String companyCountry;

	@Column(name = "company_dot_number")
	@Size(max = 20, message = "Company DOT number length should be between 1 to 20")
	private String companyDot;

	@Column(name = "company_contact_number")
	@Size(max = 11, message = "Company contact number length should not be greater than 11")
	@Pattern(regexp = "^$|[0-9]*", message = "Company contact number should contain only Numeric")
	private String companyPhone;

	@ManyToOne
	@JoinColumn(name = "company_contract_type", referencedColumnName = "contract_id")
	private CompanyContractType companyContractType;

	@Column(name = "company_verified_email")
	private String companyVerifiedEmail;

	@Column(name = "company_contact_email")
	@Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message = "Enter valid Email")
	@Size(max = 50, message = "Email Id should not greater than 50 character")
	private String companyContactEmail;

	@Column(name = "company_timezone")
	private String companyTimeZone;

	@Column(name = "company_is_dst")
	private boolean companyIsDST;

	@Column(name = "company_email_verification_validity")
	private long companyEmailVerificationValidity;

	@Column(name = "company_contact_email_is_verified")
	private short companyEmailIsVerified;

	@Column(name = "company_latitude")
	private double companyLatitude;

	@Column(name = "company_longitude")
	private double companyLongitude;

	@Column(name = "company_is_eld_mandate")
	private short isELDEnabled = 1;

	@Column(name = "company_is_moa_enabled")
	private short isMoaEnabled;

	@Column(name = "company_moa_group_id")
	private int moaGroupId;

	@Column(name = "company_is_portal_tractor_management_enabled")
	private boolean companyIsPortalTractorManagementEnabled;

	@Column(name = "company_ax_number")
	private String companyAxNumber;

	@Column(name = "company_belongs_to")
	private int companyBelongsTo;

	@Column(name = "company_contract_plan")
	private String companyContractPlan;

	@Column(name = "company_contract_expiration")
	private long companyContractExpiration;

	@Column(name = "company_on_duty_dvir")
	private int onDutyDvir;

	@Column(name = "company_off_duty_dvir")
	private int offDutyDvir;

	@Column(name = "company_log_certification_number")
	private int uncertificationLogLimit;

	@Column(name = "company_rule_set")
	private String companyRuleSet = "[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\",\"18\"]";

	@Column(name = "company_enable_mail_format")
	private int companyEnableMailFormat;

	@JsonIgnore
	@Column(name = "company_last_activity", insertable = false, updatable = false)
	private String companyLastActivity;

	@Transient
	private String supportContactNumber;

	private boolean status;

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyStreetAddress() {
		return companyStreetAddress;
	}

	public void setCompanyStreetAddress(String companyStreetAddress) {
		this.companyStreetAddress = companyStreetAddress;
	}

	public String getCompanyCity() {
		return companyCity;
	}

	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}

	public String getCompanyStateProvince() {
		return companyStateProvince;
	}

	public void setCompanyStateProvince(String companyStateProvince) {
		this.companyStateProvince = companyStateProvince;
	}

	public String getCompanyZipCode() {
		return companyZipCode;
	}

	public void setCompanyZipCode(String companyZipCode) {
		this.companyZipCode = companyZipCode;
	}

	public String getCompanyCountry() {
		return companyCountry;
	}

	public void setCompanyCountry(String companyCountry) {
		this.companyCountry = companyCountry;
	}

	public String getCompanyDot() {
		return companyDot;
	}

	public void setCompanyDot(String companyDot) {
		this.companyDot = companyDot;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public CompanyContractType getCompanyContractType() {
		return companyContractType;
	}

	public void setCompanyContractType(CompanyContractType companyContractType) {
		this.companyContractType = companyContractType;
	}

	public String getCompanyVerifiedEmail() {
		return companyVerifiedEmail;
	}

	public void setCompanyVerifiedEmail(String companyVerifiedEmail) {
		this.companyVerifiedEmail = companyVerifiedEmail;
	}

	public String getCompanyContactEmail() {
		return companyContactEmail;
	}

	public void setCompanyContactEmail(String companyContactEmail) {
		this.companyContactEmail = companyContactEmail;
	}

	public String getCompanyTimeZone() {
		return companyTimeZone;
	}

	public void setCompanyTimeZone(String companyTimeZone) {
		this.companyTimeZone = companyTimeZone;
	}

	public boolean isCompanyIsDST() {
		return companyIsDST;
	}

	public void setCompanyIsDST(boolean companyIsDST) {
		this.companyIsDST = companyIsDST;
	}

	public long getCompanyEmailVerificationValidity() {
		return companyEmailVerificationValidity;
	}

	public void setCompanyEmailVerificationValidity(long companyEmailVerificationValidity) {
		this.companyEmailVerificationValidity = companyEmailVerificationValidity;
	}

	public short getCompanyEmailIsVerified() {
		return companyEmailIsVerified;
	}

	public void setCompanyEmailIsVerified(short companyEmailIsVerified) {
		this.companyEmailIsVerified = companyEmailIsVerified;
	}

	public boolean isStatus() {
		return status;
	}

	public double getCompanyLatitude() {
		return companyLatitude;
	}

	public void setCompanyLatitude(double companyLatitude) {
		this.companyLatitude = companyLatitude;
	}

	public double getCompanyLongitude() {
		return companyLongitude;
	}

	public void setCompanyLongitude(double companyLongitude) {
		this.companyLongitude = companyLongitude;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getSupportContactNumber() {
		return supportContactNumber;
	}

	public void setSupportContactNumber(String supportContactNumber) {
		this.supportContactNumber = supportContactNumber;
	}

	public short getIsELDEnabled() {
		return isELDEnabled;
	}

	public void setIsELDEnabled(short isELDEnabled) {
		this.isELDEnabled = 1;
	}

	public short getIsMoaEnabled() {
		return isMoaEnabled;
	}

	public void setIsMoaEnabled(short isMoaEnabled) {
		this.isMoaEnabled = isMoaEnabled;
	}

	public boolean isCompanyIsPortalTractorManagementEnabled() {
		return companyIsPortalTractorManagementEnabled;
	}

	public void setCompanyIsPortalTractorManagementEnabled(boolean companyIsPortalTractorManagementEnabled) {
		this.companyIsPortalTractorManagementEnabled = companyIsPortalTractorManagementEnabled;
	}

	public int getMoaGroupId() {
		return moaGroupId;
	}

	public void setMoaGroupId(int moaGroupId) {
		this.moaGroupId = moaGroupId;
	}

	public String getCompanyAxNumber() {
		return companyAxNumber;
	}

	public void setCompanyAxNumber(String companyAxNumber) {
		this.companyAxNumber = companyAxNumber;
	}

	public int getCompanyBelongsTo() {
		return companyBelongsTo;
	}

	public void setCompanyBelongsTo(int companyBelongsTo) {
		this.companyBelongsTo = companyBelongsTo;
	}

	public String getCompanyContractPlan() {
		return companyContractPlan;
	}

	public void setCompanyContractPlan(String companyContractPlan) {
		this.companyContractPlan = companyContractPlan;
	}

	public long getCompanyContractExpiration() {
		return companyContractExpiration;
	}

	public void setCompanyContractExpiration(long companyContractExpiration) {
		this.companyContractExpiration = companyContractExpiration;
	}

	public int getOnDutyDvir() {
		return onDutyDvir;
	}

	public void setOnDutyDvir(int onDutyDvir) {
		this.onDutyDvir = onDutyDvir;
	}

	public int getOffDutyDvir() {
		return offDutyDvir;
	}

	public void setOffDutyDvir(int offDutyDvir) {
		this.offDutyDvir = offDutyDvir;
	}

	public int getUncertificationLogLimit() {
		return uncertificationLogLimit;
	}

	public void setUncertificationLogLimit(int uncertificationLogLimit) {
		this.uncertificationLogLimit = uncertificationLogLimit;
	}

	public String getCompanyRuleSet() {
		return companyRuleSet;
	}

	public void setCompanyRuleSet(String companyRuleSet) {
		this.companyRuleSet = companyRuleSet;
	}

	public int getCompanyEnableMailFormat() {
		return companyEnableMailFormat;
	}

	public void setCompanyEnableMailFormat(int companyEnableMailFormat) {
		this.companyEnableMailFormat = companyEnableMailFormat;
	}

	public String getCompanyLastActivity() {
		return companyLastActivity;
	}

	public void setCompanyLastActivity(String companyLastActivity) {
		this.companyLastActivity = companyLastActivity;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
