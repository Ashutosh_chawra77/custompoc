package com.rmn.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "rm_eld_events")
@JsonIgnoreProperties(ignoreUnknown = true)
@Proxy(lazy = false)
public class EldEvents {

	@Id
	@Column(name = "eld_events_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Transient
	private int serverId;

	@Transient
	private int clientId;

	@Transient
	private String driverLoginId;

	@Column(name = "eld_events_driver_id")
	private int userId;

	@Column(name = "eld_events_record_originator_id")
	private int recordOriginatorId;

	@Column(name = "eld_events_company_id")
	private int companyId;

	@Transient
	private String companyCode;

	@Column(name = "eld_events_device_serial_number")
	private String deviceSerialNumber;

	@Column(name = "eld_events_hos_id")
	private int hosId;

	@Column(name = "eld_events_vin")
	private String vin;

	@Column(name = "eld_events_tractor_number")
	private String tractorNumber;

	@Column(name = "eld_events_trailer_number")
	private String trailerNumber;

	@Column(name = "eld_events_shipping_doc_number")
	private String shippingDocNumber;

	@ManyToOne
	@JoinColumn(name = "eld_events_eld_events_description_id", nullable = false, referencedColumnName = "eld_events_description_id")
	private EldEventsDescription eldEventsDescription;

	@Transient
	private short type;

	@Transient
	private short code;

	@Transient
	private String eldDescription;

	@Transient
	private String dutyStatus;

	@Transient
	private String date;

	@Transient
	private String time;

	@Column(name = "eld_events_timestamp")
	private long eventTs;

	@Column(name = "eld_events_finished_timestamp")
	private long eventFinishedTimestamp;

	@Column(name = "eld_events_date_time")
	private String dateTime;

	@Column(name = "eld_events_created_timestamp")
	private long eventCreatedTs;

	@Column(name = "eld_events_utc_offset")
	private short utcOffset;

	@Column(name = "eld_events_vehicle_miles")
	private double vehicleMiles;

	@Column(name = "eld_events_engine_hours")
	private double engineHours;

	@Column(name = "eld_events_latitude")
	private String latitude;

	@Column(name = "eld_events_longitude")
	private String longitude;

	@Column(name = "eld_events_distance")
	private double distance;

	@Transient
	private String malDiagCode;

	@Column(name = "eld_events_mal_indicator_status")
	private short malIndicatorStatus;

	@Column(name = "eld_events_diag_indicator_status")
	private short diagIndicatorStatus;

	@Column(name = "eld_events_comment")
	private String comment;

	@Column(name = "eld_events_location")
	private String location;

	@Column(name = "eld_events_data_check_value")
	private String dataCheckValue;

	@Column(name = "eld_events_is_changed")
	private short isChanged;

	@Column(name = "eld_events_record_origin")
	private short recordOrigin;

	@Column(name = "eld_events_record_status")
	private short recordStatus;

	@Column(name = "eld_events_is_active")
	private short isActive;

	@Column(name = "eld_events_offduty_time_deferred")
	private String offdutyTimeDeferred;

//    @Column(name = "eld_events_additional_hour_date")
//    private String additionalHourDate;

	@Column(name = "eld_events_start_workshift_time")
	private String startWorkshiftTime;

	@Column(name = "eld_events_end_workshift_time")
	private String endWorkshiftTime;

	@Column(name = "eld_events_total_hours_offduty")
	private String totalHoursOffduty;

	@Column(name = "eld_events_total_hours_onduty")
	private String totalHoursOnduty;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getServerId() {
		return serverId;
	}

	public void setServerId(int serverId) {
		this.serverId = serverId;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getDriverLoginId() {
		return driverLoginId;
	}

	public void setDriverLoginId(String driverLoginId) {
		this.driverLoginId = driverLoginId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getRecordOriginatorId() {
		return recordOriginatorId;
	}

	public void setRecordOriginatorId(int recordOriginatorId) {
		this.recordOriginatorId = recordOriginatorId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getDeviceSerialNumber() {
		return deviceSerialNumber;
	}

	public void setDeviceSerialNumber(String deviceSerialNumber) {
		this.deviceSerialNumber = deviceSerialNumber;
	}

	public int getHosId() {
		return hosId;
	}

	public void setHosId(int hosId) {
		this.hosId = hosId;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getTractorNumber() {
		return tractorNumber;
	}

	public void setTractorNumber(String tractorNumber) {
		this.tractorNumber = tractorNumber;
	}

	public String getTrailerNumber() {
		return trailerNumber;
	}

	public void setTrailerNumber(String trailerNumber) {
		this.trailerNumber = trailerNumber;
	}

	public String getShippingDocNumber() {
		return shippingDocNumber;
	}

	public void setShippingDocNumber(String shippingDocNumber) {
		this.shippingDocNumber = shippingDocNumber;
	}

//    public String getSequenceId() {
//        return sequenceId;
//    }
//
//    public void setSequenceId(String sequenceId) {
//        this.sequenceId = sequenceId;
//    }

	public EldEventsDescription getEldEventsDescription() {
		return eldEventsDescription;
	}

	public void setEldEventsDescription(EldEventsDescription eldEventsDescription) {
		this.eldEventsDescription = eldEventsDescription;
	}

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public short getCode() {
		return code;
	}

	public void setCode(short code) {
		this.code = code;
	}

	public String getEldDescription() {
		return eldDescription;
	}

	public void setEldDescription(String eldDescription) {
		this.eldDescription = eldDescription;
	}

	public String getDutyStatus() {
		return dutyStatus;
	}

	public void setDutyStatus(String dutyStatus) {
		this.dutyStatus = dutyStatus;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public long getEventTs() {
		return eventTs;
	}

	public void setEventTs(long eventTs) {
		this.eventTs = eventTs;
	}

	public long getEventFinishedTimestamp() {
		return eventFinishedTimestamp;
	}

	public void setEventFinishedTimestamp(long eventFinishedTimestamp) {
		this.eventFinishedTimestamp = eventFinishedTimestamp;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public long getEventCreatedTs() {
		return eventCreatedTs;
	}

	public void setEventCreatedTs(long eventCreatedTs) {
		this.eventCreatedTs = eventCreatedTs;
	}

	public short getUtcOffset() {
		return utcOffset;
	}

	public void setUtcOffset(short utcOffset) {
		this.utcOffset = utcOffset;
	}

	public double getVehicleMiles() {
		return vehicleMiles;
	}

	public void setVehicleMiles(double vehicleMiles) {
		this.vehicleMiles = vehicleMiles;
	}

	public double getEngineHours() {
		return engineHours;
	}

	public void setEngineHours(double engineHours) {
		this.engineHours = engineHours;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getMalDiagCode() {
		return malDiagCode;
	}

	public void setMalDiagCode(String malDiagCode) {
		this.malDiagCode = malDiagCode;
	}

	public short getMalIndicatorStatus() {
		return malIndicatorStatus;
	}

	public void setMalIndicatorStatus(short malIndicatorStatus) {
		this.malIndicatorStatus = malIndicatorStatus;
	}

	public short getDiagIndicatorStatus() {
		return diagIndicatorStatus;
	}

	public void setDiagIndicatorStatus(short diagIndicatorStatus) {
		this.diagIndicatorStatus = diagIndicatorStatus;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDataCheckValue() {
		return dataCheckValue;
	}

	public void setDataCheckValue(String dataCheckValue) {
		this.dataCheckValue = dataCheckValue;
	}

	public short getIsChanged() {
		return isChanged;
	}

	public void setIsChanged(short isChanged) {
		this.isChanged = isChanged;
	}

	public short getRecordOrigin() {
		return recordOrigin;
	}

	public void setRecordOrigin(short recordOrigin) {
		this.recordOrigin = recordOrigin;
	}

	public short getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(short recordStatus) {
		this.recordStatus = recordStatus;
	}

	public short getIsActive() {
		return isActive;
	}

	public void setIsActive(short isActive) {
		this.isActive = isActive;
	}

//    public EldEventsSequence getEldEventsSequence() {
//        return eldEventsSequence;
//    }
//
//    public void setEldEventsSequence(EldEventsSequence eldEventsSequence) {
//        this.eldEventsSequence = eldEventsSequence;
//    }

	public String getOffdutyTimeDeferred() {
		return offdutyTimeDeferred;
	}

	public void setOffdutyTimeDeferred(String offdutyTimeDeferred) {
		this.offdutyTimeDeferred = offdutyTimeDeferred;
	}

	public String getStartWorkshiftTime() {
		return startWorkshiftTime;
	}

	public void setStartWorkshiftTime(String startWorkshiftTime) {
		this.startWorkshiftTime = startWorkshiftTime;
	}

	public String getEndWorkshiftTime() {
		return endWorkshiftTime;
	}

	public void setEndWorkshiftTime(String endWorkshiftTime) {
		this.endWorkshiftTime = endWorkshiftTime;
	}

	public String getTotalHoursOffduty() {
		return totalHoursOffduty;
	}

	public void setTotalHoursOffduty(String totalHoursOffduty) {
		this.totalHoursOffduty = totalHoursOffduty;
	}

	public String getTotalHoursOnduty() {
		return totalHoursOnduty;
	}

	public void setTotalHoursOnduty(String totalHoursOnduty) {
		this.totalHoursOnduty = totalHoursOnduty;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
